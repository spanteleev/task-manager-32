package ru.tsc.panteleev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.panteleev.tm.api.service.ITaskService;
import ru.tsc.panteleev.tm.api.service.IServiceLocator;
import ru.tsc.panteleev.tm.dto.request.task.*;
import ru.tsc.panteleev.tm.dto.response.task.*;
import ru.tsc.panteleev.tm.enumerated.Sort;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.model.Task;

import java.util.List;

public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @NotNull
    @Override
    public TaskCreateResponse createTask(@NotNull TaskCreateRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable Task task = getTaskService().create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @Override
    public @NotNull TaskListResponse listTask(@NotNull TaskListRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Sort sort = request.getSort();
        @NotNull final List<Task> tasks = getTaskService().findAll(userId, sort);
        return new TaskListResponse(tasks);
    }

    @Override
    public @NotNull TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull TaskChangeStatusByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable Task task = getTaskService().changeStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @Override
    public @NotNull TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable Task task = getTaskService().changeStatusByIndex(userId, index, status);
        return new TaskChangeStatusByIndexResponse(task);
    }

    @Override
    public @NotNull TaskClearResponse clearTask(@NotNull TaskClearRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        getTaskService().clear(userId);
        return new TaskClearResponse();
    }

    @Override
    public @NotNull TaskCompleteByIdResponse completeTaskStatusById(@NotNull TaskCompleteByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable Task task = getTaskService().changeStatusById(userId, id, Status.COMPLETED);
        return new TaskCompleteByIdResponse(task);
    }

    @Override
    public @NotNull TaskCompleteByIndexResponse completeTaskStatusByIndex(@NotNull TaskCompleteByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable Task task = getTaskService().changeStatusByIndex(userId, index, Status.COMPLETED);
        return new TaskCompleteByIndexResponse(task);
    }

    @Override
    public @NotNull TaskRemoveByIdResponse removeTaskById(@NotNull TaskRemoveByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable Task task = getTaskService().removeById(userId, id);
        return new TaskRemoveByIdResponse(task);
    }

    @Override
    public @NotNull TaskRemoveByIndexResponse removeTaskByIndex(@NotNull TaskRemoveByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable Task task = getTaskService().removeByIndex(userId, index);
        return new TaskRemoveByIndexResponse(task);
    }

    @Override
    public @NotNull TaskShowByIdResponse showTaskById(@NotNull TaskShowByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable Task task = getTaskService().findById(userId, id);
        return new TaskShowByIdResponse(task);
    }

    @Override
    public @NotNull TaskShowByIndexResponse showTaskByIndex(@NotNull TaskShowByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable Task task = getTaskService().findByIndex(userId, index);
        return new TaskShowByIndexResponse(task);
    }

    @Override
    public @NotNull TaskStartByIdResponse startTaskById(@NotNull TaskStartByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable Task task = getTaskService().changeStatusById(userId, id, Status.IN_PROGRESS);
        return new TaskStartByIdResponse(task);
    }

    @Override
    public @NotNull TaskStartByIndexResponse startTaskByIndex(@NotNull TaskStartByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable Task task = getTaskService().changeStatusByIndex(userId, index, Status.IN_PROGRESS);
        return new TaskStartByIndexResponse(task);
    }

    @Override
    public @NotNull TaskUpdateByIdResponse updateTaskById(@NotNull TaskUpdateByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable Task task = getTaskService().updateById(userId, id, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @Override
    public @NotNull TaskUpdateByIndexResponse updateTaskByIndex(@NotNull TaskUpdateByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable Task task = getTaskService().updateByIndex(userId, index, name, description);
        return new TaskUpdateByIndexResponse(task);
    }

    @Override
    public @NotNull TaskShowListByProjectIdResponse showListByProjectIdTask(@NotNull TaskShowListByProjectIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @NotNull final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectId);
        return new TaskShowListByProjectIdResponse(tasks);
    }

}
