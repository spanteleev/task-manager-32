package ru.tsc.panteleev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.endpoint.IUserEndpoint;
import ru.tsc.panteleev.tm.api.service.IServiceLocator;
import ru.tsc.panteleev.tm.api.service.ITaskService;
import ru.tsc.panteleev.tm.api.service.IUserService;
import ru.tsc.panteleev.tm.dto.request.user.*;
import ru.tsc.panteleev.tm.dto.response.user.*;

public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserService getUserService() {
        return getServiceLocator().getUserService();
    }


    @Override
    public UserChangePasswordResponse changePasswordUser(@NotNull UserChangePasswordRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable String password = request.getPassword();
        getUserService().setPassword(userId, password);
        return new UserChangePasswordResponse();

    }

    @Override
    public UserLockResponse lockUser(@NotNull UserLockRequest request) {
        check(request);
        @Nullable String login = request.getLogin();
        getUserService().lockUserByLogin(login);
        return new UserLockResponse();
    }

    @Override
    public UserRegistryResponse registrationUser(@NotNull UserRegistryRequest request) {
        @Nullable String login = request.getLogin();
        @Nullable String password = request.getPassword();
        @Nullable String email = request.getEmail();
        getUserService().create(login, password, email);
        return new UserRegistryResponse();
    }

    @Override
    public UserRemoveResponse removeUser(@NotNull UserRemoveRequest request) {
        check(request);
        @Nullable String login = request.getLogin();
        getUserService().removeByLogin(login);
        return new UserRemoveResponse();
    }

    @Override
    public UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request) {
        check(request);
        @Nullable String login = request.getLogin();
        getUserService().unlockUserByLogin(login);
        return new UserUnlockResponse();
    }

    @Override
    public UserUpdateResponse updateUser(@NotNull UserUpdateRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable String firstName = request.getFirstName();
        @Nullable String lastName = request.getLastName();
        @Nullable String middleName = request.getMiddleName();
        getUserService().updateUser(userId, firstName, lastName, middleName);
        return new UserUpdateResponse();
    }
}
