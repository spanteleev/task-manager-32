package ru.tsc.panteleev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.repository.ITaskRepository;
import ru.tsc.panteleev.tm.api.repository.IUserOwnedRepository;
import ru.tsc.panteleev.tm.api.service.ITaskService;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.panteleev.tm.exception.field.*;
import ru.tsc.panteleev.tm.model.Task;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@Nullable final ITaskRepository repository) {
        super(repository);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        return repository.findAllByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    public Task create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(userId, name);
    }

    @NotNull
    @Override
    public Task create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }

    @NotNull
    @Override
    public Task create(@Nullable final String userId, @Nullable String name, @Nullable String description, @Nullable Date dateBegin, @Nullable Date dateEnd) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        final Task task = Optional.ofNullable(create(userId, name, description))
                .orElseThrow(TaskNotFoundException::new);
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        return task;
    }

    @NotNull
    @Override
    public Task updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final Task task = findByIndex(index);
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Task task = findById(id);
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final Task task = findByIndex(index);
        if (status == null) throw new StatusIncorrectException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Task task = findById(id);
        if (status == null) throw new StatusIncorrectException();
        task.setStatus(status);
        return task;
    }

}
