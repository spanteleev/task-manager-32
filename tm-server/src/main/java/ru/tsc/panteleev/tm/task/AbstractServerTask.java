package ru.tsc.panteleev.tm.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.component.Server;

public abstract class AbstractServerTask implements Runnable {

    @NotNull
    protected Server server;

    public AbstractServerTask(@NotNull Server server) {
        this.server = server;
    }

}
