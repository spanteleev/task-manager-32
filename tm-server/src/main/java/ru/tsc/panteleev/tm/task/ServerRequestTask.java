package ru.tsc.panteleev.tm.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.service.IAuthService;
import ru.tsc.panteleev.tm.api.service.IUserService;
import ru.tsc.panteleev.tm.component.Server;
import ru.tsc.panteleev.tm.dto.request.*;
import ru.tsc.panteleev.tm.dto.request.user.UserLoginRequest;
import ru.tsc.panteleev.tm.dto.request.user.UserLogoutRequest;
import ru.tsc.panteleev.tm.dto.request.user.UserProfileRequest;
import ru.tsc.panteleev.tm.dto.response.*;
import ru.tsc.panteleev.tm.dto.response.user.UserLoginResponse;
import ru.tsc.panteleev.tm.dto.response.user.UserLogoutResponse;
import ru.tsc.panteleev.tm.dto.response.user.UserProfileResponse;
import ru.tsc.panteleev.tm.model.User;

import java.io.*;
import java.net.Socket;

public class ServerRequestTask extends AbstractServerSocketTask {

    public ServerRequestTask(@NotNull final Server server, @NotNull final Socket socket) {
        super(server, socket);
    }

    public ServerRequestTask(@NotNull final Server server, @NotNull final Socket socket, @NotNull final String userId) {
        super(server, socket, userId);
    }

    @Nullable
    private AbstractRequest request;

    @Nullable
    private AbstractResponse response;

    @Override
    @SneakyThrows
    public void run() {
        processInput();
        processUserId();
        processLogin();
        processProfile();
        processLogout();
        processOperation();
        processOutput();
    }

    @SneakyThrows
    private void processInput() {
        @NotNull final InputStream inputStream = socket.getInputStream();
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        @NotNull final Object object = objectInputStream.readObject();
        request = (AbstractRequest) object;
    }

    private void processUserId() {
        if (!(request instanceof AbstractUserRequest)) return;
        @NotNull final AbstractUserRequest abstractUserRequest = (AbstractUserRequest) request;
        abstractUserRequest.setUserId(userId);
    }

    @SneakyThrows
    private void processOutput() {
        @NotNull final OutputStream outputStream = socket.getOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(response);
        server.submit(new ServerRequestTask(server, socket, userId));
    }

    private void processLogin() {
        if (response != null) return;
        if (!(request instanceof UserLoginRequest)) return;
        try {
            @NotNull final UserLoginRequest userLoginRequest = (UserLoginRequest) request;
            @Nullable final String login = userLoginRequest.getLogin();
            @Nullable final String password = userLoginRequest.getPassword();
            @NotNull final IAuthService authService = server.getBootstrap().getAuthService();
            @NotNull final User user = authService.check(login, password);
            userId = user.getId();
            response = new UserLoginResponse();
        } catch (@NotNull final Exception e) {
            response = new UserLoginResponse(e);
        }
    }

    private void processProfile() {
        if (response != null) return;
        if (!(request instanceof UserProfileRequest)) return;
        if (userId == null) {
            response = new UserProfileResponse();
            return;
        }
        @NotNull final IUserService userService = server.getBootstrap().getUserService();
        @Nullable final User user = userService.findById(userId);
        response = new UserProfileResponse(user);
    }

    private void processLogout() {
        if (response != null) return;
        if (!(request instanceof UserLogoutRequest)) return;
        userId = null;
        response = new UserLogoutResponse();
    }

    private void processOperation() {
        if (response != null) return;
        try {
            @Nullable final Object result = server.call(request);
            response = (AbstractResponse) result;
        } catch (Exception e) {
            response = new ApplicationErrorResponse(e);
        }
    }

}
