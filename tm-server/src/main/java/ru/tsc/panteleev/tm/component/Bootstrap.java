package ru.tsc.panteleev.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.endpoint.*;
import ru.tsc.panteleev.tm.api.repository.IProjectRepository;
import ru.tsc.panteleev.tm.api.repository.ITaskRepository;
import ru.tsc.panteleev.tm.api.repository.IUserRepository;
import ru.tsc.panteleev.tm.api.service.*;
import ru.tsc.panteleev.tm.dto.request.data.*;
import ru.tsc.panteleev.tm.dto.request.project.*;
import ru.tsc.panteleev.tm.dto.request.task.*;
import ru.tsc.panteleev.tm.dto.request.user.*;
import ru.tsc.panteleev.tm.endpoint.*;
import ru.tsc.panteleev.tm.dto.request.system.ServerAboutRequest;
import ru.tsc.panteleev.tm.dto.request.system.ServerVersionRequest;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.model.User;
import ru.tsc.panteleev.tm.repository.ProjectRepository;
import ru.tsc.panteleev.tm.repository.TaskRepository;
import ru.tsc.panteleev.tm.repository.UserRepository;
import ru.tsc.panteleev.tm.service.*;
import ru.tsc.panteleev.tm.util.DateUtil;
import ru.tsc.panteleev.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(taskService, projectService, userService);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    {

        server.reqistry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.reqistry(ServerVersionRequest.class, systemEndpoint::getVersion);

        server.reqistry(ProjectChangeStatusByIdRequest.class, projectEndpoint::changeProjectStatusById);
        server.reqistry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::changeProjectStatusByIndex);
        server.reqistry(ProjectClearRequest.class, projectEndpoint::clearProject);
        server.reqistry(ProjectCompleteByIdRequest.class, projectEndpoint::completeProjectStatusById);
        server.reqistry(ProjectCompleteByIndexRequest.class, projectEndpoint::completeProjectStatusByIndex);
        server.reqistry(ProjectCreateRequest.class, projectEndpoint::createProject);
        server.reqistry(ProjectListRequest.class, projectEndpoint::listProject);
        server.reqistry(ProjectRemoveByIdRequest.class, projectEndpoint::removeProjectById);
        server.reqistry(ProjectRemoveByIndexRequest.class, projectEndpoint::removeProjectByIndex);
        server.reqistry(ProjectShowByIdRequest.class, projectEndpoint::showProjectById);
        server.reqistry(ProjectShowByIndexRequest.class, projectEndpoint::showProjectByIndex);
        server.reqistry(ProjectStartByIdRequest.class, projectEndpoint::startProjectById);
        server.reqistry(ProjectStartByIndexRequest.class, projectEndpoint::startProjectByIndex);
        server.reqistry(ProjectUpdateByIdRequest.class, projectEndpoint::updateProjectById);
        server.reqistry(ProjectUpdateByIndexRequest.class, projectEndpoint::updateProjectByIndex);

        server.reqistry(TaskChangeStatusByIdRequest.class, taskEndpoint::changeTaskStatusById);
        server.reqistry(TaskChangeStatusByIndexRequest.class, taskEndpoint::changeTaskStatusByIndex);
        server.reqistry(TaskClearRequest.class, taskEndpoint::clearTask);
        server.reqistry(TaskCompleteByIdRequest.class, taskEndpoint::completeTaskStatusById);
        server.reqistry(TaskCompleteByIndexRequest.class, taskEndpoint::completeTaskStatusByIndex);
        server.reqistry(TaskCreateRequest.class, taskEndpoint::createTask);
        server.reqistry(TaskListRequest.class, taskEndpoint::listTask);
        server.reqistry(TaskRemoveByIdRequest.class, taskEndpoint::removeTaskById);
        server.reqistry(TaskRemoveByIndexRequest.class, taskEndpoint::removeTaskByIndex);
        server.reqistry(TaskShowByIdRequest.class, taskEndpoint::showTaskById);
        server.reqistry(TaskShowByIndexRequest.class, taskEndpoint::showTaskByIndex);
        server.reqistry(TaskStartByIdRequest.class, taskEndpoint::startTaskById);
        server.reqistry(TaskStartByIndexRequest.class, taskEndpoint::startTaskByIndex);
        server.reqistry(TaskUpdateByIdRequest.class, taskEndpoint::updateTaskById);
        server.reqistry(TaskUpdateByIndexRequest.class, taskEndpoint::updateTaskByIndex);

        server.reqistry(TaskShowListByProjectIdRequest.class, taskEndpoint::showListByProjectIdTask);
        server.reqistry(TaskBindToProjectRequest.class, projectTaskEndpoint::bindToProjectTaskById);
        server.reqistry(TaskUnbindToProjectRequest.class, projectTaskEndpoint::unbindToProjectTaskById);

        server.reqistry(DataBackupLoadRequest.class, domainEndpoint::loadDataBackup);
        server.reqistry(DataBackupSaveRequest.class, domainEndpoint::saveDataBackup);
        server.reqistry(DataBase64LoadRequest.class, domainEndpoint::loadDataBase64);
        server.reqistry(DataBase64SaveRequest.class, domainEndpoint::saveDataBase64);
        server.reqistry(DataBinaryLoadRequest.class, domainEndpoint::loadDataBinary);
        server.reqistry(DataBinarySaveRequest.class, domainEndpoint::saveDataBinary);
        server.reqistry(DataJsonFasterXmlLoadRequest.class, domainEndpoint::loadDataJsonFasterXml);
        server.reqistry(DataJsonFasterXmlSaveRequest.class, domainEndpoint::saveDataJsonFasterXml);
        server.reqistry(DataJsonJaxbLoadRequest.class, domainEndpoint::loadDataJsonJaxb);
        server.reqistry(DataJsonJaxbSaveRequest.class, domainEndpoint::saveDataJsonJaxb);
        server.reqistry(DataXmlFasterXmlLoadRequest.class, domainEndpoint::loadDataXmlFasterXml);
        server.reqistry(DataXmlFasterXmlSaveRequest.class, domainEndpoint::saveDataXmlFasterXml);
        server.reqistry(DataXmlJaxbLoadRequest.class, domainEndpoint::loadDataXmlJaxb);
        server.reqistry(DataXmlJaxbSaveRequest.class, domainEndpoint::saveDataXmlJaxb);
        server.reqistry(DataYamlFasterXmlLoadRequest.class, domainEndpoint::loadDataYamlFasterXml);
        server.reqistry(DataYamlFasterXmlSaveRequest.class, domainEndpoint::saveDataYamlFasterXml);

        server.reqistry(UserChangePasswordRequest.class, userEndpoint::changePasswordUser);
        server.reqistry(UserRegistryRequest.class, userEndpoint::registrationUser);
        server.reqistry(UserLockRequest.class, userEndpoint::lockUser);
        server.reqistry(UserUnlockRequest.class, userEndpoint::unlockUser);
        server.reqistry(UserRemoveRequest.class, userEndpoint::removeUser);
        server.reqistry(UserUpdateRequest.class, userEndpoint::updateUser);

    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void runStartupOperation() {
        initPID();
        backup.start();
        initDemoData();
        registryShutdownHookOperation();
        server.start();
        loggerService.info("** TASK-MANAGER SERVER IS STARTING **");
    }

    private void registryShutdownHookOperation() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER SERVER IS SHUTTING DOWN **");
                backup.stop();
            }
        });
    }

    private void initDemoData() {
        if (userService.getSize() > 0) return;
        userService.create("test", "test", "qwerty@qwerty.ru");
        userService.create("admin", "admin", Role.ADMIN);
        @Nullable final User admin = userService.findByLogin("admin");
        @Nullable final User test = userService.findByLogin("test");
        if (admin == null || test == null) return;
        @NotNull final String adminId = admin.getId();
        @NotNull final String testId = test.getId();
        projectService.create(adminId, "p3", "333",
                DateUtil.toDate("29.08.2021"), DateUtil.toDate("29.08.2022"));
        projectService.create(adminId, "p1", "111");
        projectService.create(testId, "p5", "555",
                DateUtil.toDate("29.08.2021"), DateUtil.toDate("29.08.2022"));
        projectService.create(testId, "p4", "444");
        projectService.create(testId, "p6", "666");
        taskService.create(adminId, "t3", "333333",
                DateUtil.toDate("29.08.2021"), DateUtil.toDate("29.08.2022"));
        taskService.create(adminId, "t1", "111111");
        taskService.create(testId, "t5", "555555",
                DateUtil.toDate("29.08.2021"), DateUtil.toDate("29.08.2022"));
        taskService.create(testId, "t4", "444444");
        taskService.create(testId, "t6", "666666");
    }

    public void run() {
        runStartupOperation();
    }


}
