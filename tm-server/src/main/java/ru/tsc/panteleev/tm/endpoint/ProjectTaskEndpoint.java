package ru.tsc.panteleev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.panteleev.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.panteleev.tm.api.service.IProjectTaskService;
import ru.tsc.panteleev.tm.api.service.IServiceLocator;
import ru.tsc.panteleev.tm.api.service.ITaskService;
import ru.tsc.panteleev.tm.dto.request.task.*;
import ru.tsc.panteleev.tm.dto.response.task.*;
import ru.tsc.panteleev.tm.enumerated.Sort;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.model.Task;

import java.util.List;

public class ProjectTaskEndpoint extends AbstractEndpoint implements IProjectTaskEndpoint {

    public ProjectTaskEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @Override
    public @NotNull TaskBindToProjectResponse bindToProjectTaskById(@NotNull TaskBindToProjectRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new TaskBindToProjectResponse();
    }

    @Override
    public @NotNull TaskUnbindToProjectResponse unbindToProjectTaskById(@NotNull TaskUnbindToProjectRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new TaskUnbindToProjectResponse();
    }

}
