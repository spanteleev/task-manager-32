package ru.tsc.panteleev.tm.api.model;

import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.enumerated.Status;

public interface IHasStatus {

    @Nullable
    Status getStatus();

    void setStatus(Status status);

}
