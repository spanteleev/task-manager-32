package ru.tsc.panteleev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.client.DomainEndpointClient;
import ru.tsc.panteleev.tm.command.AbstractCommand;
import ru.tsc.panteleev.tm.enumerated.Role;

public abstract class AbstractDataCommand extends AbstractCommand {


    @NotNull
    public DomainEndpointClient getDomainEndpoint() {
        return serviceLocator.getDomainEndpoint();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showDescription() {
        System.out.println("[" + getDescription().toUpperCase() + "]");
    }

}
