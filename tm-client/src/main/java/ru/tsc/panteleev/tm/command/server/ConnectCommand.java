package ru.tsc.panteleev.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.command.AbstractCommand;
import ru.tsc.panteleev.tm.enumerated.Role;

import java.net.Socket;

public class ConnectCommand extends AbstractCommand {

    @NotNull
    public static final String NAME = "connect";

    @NotNull
    public static final String DESCRIPTION = "Connect to server";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    @SneakyThrows
    public void execute() {
        serviceLocator.getAuthEndpoint().connect();
        Socket socket = serviceLocator.getAuthEndpoint().getSocket();
        serviceLocator.getSystemEndpoint().setSocket(socket);
        serviceLocator.getProjectEndpoint().setSocket(socket);
        serviceLocator.getTaskEndpoint().setSocket(socket);
        serviceLocator.getProjectTaskEndpoint().setSocket(socket);
        serviceLocator.getDomainEndpoint().setSocket(socket);
        serviceLocator.getUserEndpoint().setSocket(socket);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }
}
