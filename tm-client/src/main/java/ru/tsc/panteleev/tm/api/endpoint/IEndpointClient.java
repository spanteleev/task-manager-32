package ru.tsc.panteleev.tm.api.endpoint;

import java.io.IOException;

public interface IEndpointClient {

    void connect() throws IOException;

    void disconnect() throws IOException;

}
