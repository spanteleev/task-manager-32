package ru.tsc.panteleev.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.panteleev.tm.api.endpoint.*;
import ru.tsc.panteleev.tm.api.repository.ICommandRepository;
import ru.tsc.panteleev.tm.api.service.*;
import ru.tsc.panteleev.tm.client.*;
import ru.tsc.panteleev.tm.command.AbstractCommand;
//import ru.tsc.panteleev.tm.exception.system.ArgumentNotSupportedException;
//import ru.tsc.panteleev.tm.exception.system.CommandNotSupportedException;
import ru.tsc.panteleev.tm.exception.system.CommandNotSupportedException;
import ru.tsc.panteleev.tm.repository.CommandRepository;
import ru.tsc.panteleev.tm.service.*;
import ru.tsc.panteleev.tm.util.SystemUtil;
import ru.tsc.panteleev.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.tsc.panteleev.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final AuthEndpointClient authEndpoint = new AuthEndpointClient();

    @Getter
    @NotNull
    private final ProjectEndpointClient projectEndpoint = new ProjectEndpointClient();

    @Getter
    @NotNull
    private final TaskEndpointClient taskEndpoint = new TaskEndpointClient();

    @Getter
    @NotNull
    private final ProjectTaskEndpointClient projectTaskEndpoint = new ProjectTaskEndpointClient();

    @Getter
    @NotNull
    private final SystemEndpointClient systemEndpoint = new SystemEndpointClient();

    @Getter
    @NotNull
    private final UserEndpointClient userEndpoint = new UserEndpointClient();

    @Getter
    @NotNull
    private final DomainEndpointClient domainEndpoint = new DomainEndpointClient();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.tsc.panteleev.tm.command.AbstractCommand.class);
        classes.stream().filter(clazz -> !Modifier.isAbstract(clazz.getModifiers())
                && AbstractCommand.class.isAssignableFrom(clazz))
                .forEach(this::registry);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        @NotNull final AbstractCommand command = clazz.newInstance();
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void runStartupOperation() {
        initPID();

        fileScanner.start();
        registryShutdownHookOperation();

        loggerService.info("** WELCOME TO TASK-MANAGER **");
    }

    private void registryShutdownHookOperation() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
                fileScanner.stop();
            }
        });
    }

    public void run(@Nullable String[] args) {
        if (runWithArgument(args))
            System.exit(0);
        runStartupOperation();
        while (true)
            runWithCommand();
    }

    public void runWithCommand() {
        try {
            System.out.println("ENTER COMMAND:");
            String command = TerminalUtil.nextLine();
            runWithCommand(command);
            loggerService.command(command);
        } catch (final Exception e) {
            loggerService.error(e);
        }
    }

    /*public void runWithCommand(@Nullable String command) {
        runWithCommand(command, true);
    }*/


    protected void runWithCommand(@Nullable String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    public boolean runWithArgument(@Nullable String[] args) {
        if (args == null || args.length == 0) return false;
        runWithArgument(args[0]);
        return true;
    }

    public void runWithArgument(@Nullable String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(argument);
        //  if (abstractCommand == null)
        //      throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    @Override
    public @NotNull TaskEndpointClient getTaskEndpoint() {
        return taskEndpoint;
    }

    @Override
    public @NotNull ProjectEndpointClient getProjectEndpoint() {
        return projectEndpoint;
    }

    @Override
    public @NotNull ProjectTaskEndpointClient getProjectTaskEndpoint() {
        return projectTaskEndpoint;
    }

    @Override
    public @NotNull AuthEndpointClient getAuthEndpoint() {
        return authEndpoint;
    }

    @Override
    public @NotNull SystemEndpointClient getSystemEndpoint() {
        return systemEndpoint;
    }

    @Override
    public @NotNull UserEndpointClient getUserEndpoint() {
        return userEndpoint;
    }

    @Override
    public @NotNull DomainEndpointClient getDomainEndpoint() {
        return domainEndpoint;
    }
}
