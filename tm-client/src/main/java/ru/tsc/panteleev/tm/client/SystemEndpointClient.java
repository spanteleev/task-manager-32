package ru.tsc.panteleev.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.panteleev.tm.dto.request.system.ServerAboutRequest;
import ru.tsc.panteleev.tm.dto.request.system.ServerVersionRequest;
import ru.tsc.panteleev.tm.dto.response.system.ServerAboutResponse;
import ru.tsc.panteleev.tm.dto.response.system.ServerVersionResponse;

public class SystemEndpointClient extends AbstractEndpoint implements ISystemEndpoint {

    @NotNull
    @Override
    @SneakyThrows
    public ServerAboutResponse getAbout(ServerAboutRequest request) {
        return (ServerAboutResponse) call(request);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerVersionResponse getVersion(ServerVersionRequest request) {
        return (ServerVersionResponse) call(request);
    }

}
