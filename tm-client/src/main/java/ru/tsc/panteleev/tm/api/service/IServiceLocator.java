package ru.tsc.panteleev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.api.endpoint.*;
import ru.tsc.panteleev.tm.client.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    TaskEndpointClient getTaskEndpoint();

    @NotNull
    ProjectEndpointClient getProjectEndpoint();

    @NotNull
    ProjectTaskEndpointClient getProjectTaskEndpoint();

    @NotNull
    SystemEndpointClient getSystemEndpoint();

    @NotNull
    UserEndpointClient getUserEndpoint();

    @NotNull
    DomainEndpointClient getDomainEndpoint();

    @NotNull
    AuthEndpointClient getAuthEndpoint();


}
