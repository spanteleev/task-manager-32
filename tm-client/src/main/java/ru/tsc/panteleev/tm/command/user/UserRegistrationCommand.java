package ru.tsc.panteleev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.request.user.UserRegistryRequest;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.util.TerminalUtil;

public class UserRegistrationCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-registry-profile";

    @NotNull
    public static final String DESCRIPTION = "Registration user profile.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRATION PROFILE]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER E-MAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        getUserService().registrationUser(new UserRegistryRequest(login, password, email));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
