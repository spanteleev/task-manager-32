package ru.tsc.panteleev.tm.api.endpoint;

import ru.tsc.panteleev.tm.dto.request.data.*;
import ru.tsc.panteleev.tm.dto.response.data.*;

public interface IDomainEndpoint {

    DataBackupLoadResponse loadDataBackup(DataBackupLoadRequest request);

    DataBackupSaveResponse saveDataBackup(DataBackupSaveRequest request);

    DataBase64LoadResponse loadDataBase64(DataBase64LoadRequest request);

    DataBase64SaveResponse saveDataBase64(DataBase64SaveRequest request);

    DataBinaryLoadResponse loadDataBinary(DataBinaryLoadRequest request);

    DataBinarySaveResponse saveDataBinary(DataBinarySaveRequest request);

    DataJsonFasterXmlLoadResponse loadDataJsonFasterXml(DataJsonFasterXmlLoadRequest request);

    DataJsonFasterXmlSaveResponse saveDataJsonFasterXml(DataJsonFasterXmlSaveRequest request);

    DataJsonJaxbLoadResponse loadDataJsonJaxb(DataJsonJaxbLoadRequest request);

    DataJsonJaxbSaveResponse saveDataJsonJaxb(DataJsonJaxbSaveRequest request);

    DataXmlFasterXmlLoadResponse loadDataXmlFasterXml(DataXmlFasterXmlLoadRequest request);

    DataXmlFasterXmlSaveResponse saveDataXmlFasterXml(DataXmlFasterXmlSaveRequest request);

    DataXmlJaxbLoadResponse loadDataXmlJaxb(DataXmlJaxbLoadRequest request);

    DataXmlJaxbSaveResponse saveDataXmlJaxb(DataXmlJaxbSaveRequest request);

    DataYamlFasterXmlLoadResponse loadDataYamlFasterXml(DataYamlFasterXmlLoadRequest request);

    DataYamlFasterXmlSaveResponse saveDataYamlFasterXml(DataYamlFasterXmlSaveRequest request);

}
