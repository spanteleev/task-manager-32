package ru.tsc.panteleev.tm.dto.request.task;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.request.AbstractUserRequest;
import ru.tsc.panteleev.tm.enumerated.Sort;

@Getter
@Setter
@NoArgsConstructor
public class TaskListRequest extends AbstractUserRequest {

    @Nullable
    private Sort sort;

    public TaskListRequest(Sort sort) {
        this.sort = sort;
    }

}
