package ru.tsc.panteleev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.request.user.*;
import ru.tsc.panteleev.tm.dto.response.user.*;

public interface IUserEndpoint {

    UserChangePasswordResponse changePasswordUser(@NotNull UserChangePasswordRequest request);

    UserLockResponse lockUser(@NotNull UserLockRequest request);

    UserRegistryResponse registrationUser(@NotNull UserRegistryRequest request);

    UserRemoveResponse removeUser(@NotNull UserRemoveRequest request);

    UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request);

    UserUpdateResponse updateUser(@NotNull UserUpdateRequest request);

}
