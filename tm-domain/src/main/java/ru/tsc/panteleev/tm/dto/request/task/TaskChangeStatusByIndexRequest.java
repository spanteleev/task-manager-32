package ru.tsc.panteleev.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.request.AbstractUserRequest;
import ru.tsc.panteleev.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public class TaskChangeStatusByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    @Nullable
    private Status status;

    public TaskChangeStatusByIndexRequest(@Nullable Integer index, @Nullable Status status) {
        this.index = index;
        this.status = status;
    }

}
