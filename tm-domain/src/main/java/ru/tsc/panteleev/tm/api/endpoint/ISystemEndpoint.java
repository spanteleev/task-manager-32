package ru.tsc.panteleev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.request.system.ServerAboutRequest;
import ru.tsc.panteleev.tm.dto.request.system.ServerVersionRequest;
import ru.tsc.panteleev.tm.dto.response.system.ServerAboutResponse;
import ru.tsc.panteleev.tm.dto.response.system.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(ServerAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(ServerVersionRequest request);
}
