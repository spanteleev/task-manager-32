package ru.tsc.panteleev.tm.api.endpoint;

import ru.tsc.panteleev.tm.dto.request.AbstractRequest;
import ru.tsc.panteleev.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}
