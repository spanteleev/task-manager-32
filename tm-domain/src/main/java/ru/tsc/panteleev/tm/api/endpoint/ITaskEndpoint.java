package ru.tsc.panteleev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.request.task.*;
import ru.tsc.panteleev.tm.dto.response.task.*;

public interface ITaskEndpoint {

    @NotNull TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull TaskChangeStatusByIdRequest request);

    @NotNull TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request);

    @NotNull TaskClearResponse clearTask(@NotNull TaskClearRequest request);

    @NotNull TaskCompleteByIdResponse completeTaskStatusById(@NotNull TaskCompleteByIdRequest request);

    @NotNull TaskCompleteByIndexResponse completeTaskStatusByIndex(@NotNull TaskCompleteByIndexRequest request);

    @NotNull TaskCreateResponse createTask(@NotNull TaskCreateRequest request);

    @NotNull TaskListResponse listTask(@NotNull TaskListRequest request);

    @NotNull TaskRemoveByIdResponse removeTaskById(@NotNull TaskRemoveByIdRequest request);

    @NotNull TaskRemoveByIndexResponse removeTaskByIndex(@NotNull TaskRemoveByIndexRequest request);

    @NotNull TaskShowByIdResponse showTaskById(@NotNull TaskShowByIdRequest request);

    @NotNull TaskShowByIndexResponse showTaskByIndex(@NotNull TaskShowByIndexRequest request);

    @NotNull TaskStartByIdResponse startTaskById(@NotNull TaskStartByIdRequest request);

    @NotNull TaskStartByIndexResponse startTaskByIndex(@NotNull TaskStartByIndexRequest request);

    @NotNull TaskUpdateByIdResponse updateTaskById(@NotNull TaskUpdateByIdRequest request);

    @NotNull TaskUpdateByIndexResponse updateTaskByIndex(@NotNull TaskUpdateByIndexRequest request);

    @NotNull TaskShowListByProjectIdResponse showListByProjectIdTask(@NotNull TaskShowListByProjectIdRequest request);

}
