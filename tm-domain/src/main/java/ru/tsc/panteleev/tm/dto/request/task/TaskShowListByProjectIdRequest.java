package ru.tsc.panteleev.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class TaskShowListByProjectIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public TaskShowListByProjectIdRequest(@Nullable String projectId) {
        this.projectId = projectId;
    }

}
