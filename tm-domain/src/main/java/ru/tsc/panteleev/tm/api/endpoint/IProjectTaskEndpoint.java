package ru.tsc.panteleev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.request.task.*;
import ru.tsc.panteleev.tm.dto.response.task.*;

public interface IProjectTaskEndpoint {

    @NotNull TaskBindToProjectResponse bindToProjectTaskById(@NotNull TaskBindToProjectRequest request);

    @NotNull TaskUnbindToProjectResponse unbindToProjectTaskById(@NotNull TaskUnbindToProjectRequest request);

}
