package ru.tsc.panteleev.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.model.Project;

@NoArgsConstructor
public class ProjectShowByIndexResponse extends AbstractProjectResponse {

    public ProjectShowByIndexResponse(@Nullable Project project) {
        super(project);
    }

}
